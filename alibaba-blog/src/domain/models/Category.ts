import { IEntity } from "./../shared/IEntity";

export class Category implements IEntity {
  id?: number;
  title: string;

  constructor(title: string, id?: number) {
    this.title = title;
    this.id = id;
  }

  equals(entity: IEntity): boolean {
    throw new Error("Method not implemented.");
  }
}
