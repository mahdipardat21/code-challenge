import { BadRequestException } from "@nestjs/common";
import { IEntity } from "./../shared/IEntity";
import { Category } from "./Category";
import { User } from "./User";

export class Post implements IEntity {
  id?: number;
  title: string;
  text: string;
  user?: User;
  categories?: Category[];
  createdAt?: Date;
  updatedAt?: Date;

  constructor(
    title: string,
    text: string,
    user?: User,
    categories?: Category[],
    id?: number
  ) {
    this.title = title;
    this.text = text;
    this.user = user;
    if (categories.length > 6) {
      throw new BadRequestException("categories can not more than 6");
    }
    this.categories = categories;
    this.id = id;
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof Post)) return false;

    return this.id === entity.id;
  }
}
