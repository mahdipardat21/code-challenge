import {
  ArgumentsHost,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from "@nestjs/common";

@Injectable()
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    if (exception instanceof HttpException) {
      const statusCode = exception.getStatus();

      if (statusCode !== HttpStatus.UNPROCESSABLE_ENTITY)
        return response.status(statusCode).json({
          statusCode,
          message: exception.message,
          timestamp: new Date().toISOString(),
          path: request.url,
        });

      const exceptionResponse: any = exception.getResponse();
      console.log(exceptionResponse);

      return response.status(statusCode).json({
        statusCode,
        error: exceptionResponse.message,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    }
    return response.status(500).json({
      statusCode: 500,
      error: exception["message"],
      timestamp: new Date().toISOString(),
      path: null,
    });
  }
}
