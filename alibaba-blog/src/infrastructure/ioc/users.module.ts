import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { IUsersRepository } from "src/application/ports/IUsersRepository";
import { UsersUseCases } from "src/application/use-cases/UsersUseCases";
import { UserEntity } from "../database/mapper/UserEntity";
import { UsersRepository } from "../database/repositories/UsersRepostiroy";

@Module({
  imports: [SequelizeModule.forFeature([UserEntity])],
  controllers: [],
  providers: [
    UsersUseCases,
    {
      provide: IUsersRepository,
      useClass: UsersRepository,
    },
  ],
  exports: [
    UsersUseCases
  ]
})
export class UsersModule {}
