import { IUsersRepository } from "src/application/ports/IUsersRepository";
import { Module } from "@nestjs/common";
import { PostsUseCases } from "src/application/use-cases/PostsUseCases";
import { UsersRepository } from "../database/repositories/UsersRepostiroy";
import { IPostsRepository } from "src/application/ports/IPostsRepository";
import { PostsRepository } from "../database/repositories/PostsRepository";
import { SequelizeModule } from "@nestjs/sequelize";
import { UserEntity } from "../database/mapper/UserEntity";
import { PostEntity } from "../database/mapper/PostEntity";
import { PostsController } from "src/presentation/controllers/PostsController";
import { PostCategoriesEntity } from "../database/mapper/PostCategoriesEntity";
import { CategoryEntity } from "../database/mapper/CategoryEntity";

@Module({
  imports: [
    SequelizeModule.forFeature([
      UserEntity,
      PostEntity,
      PostCategoriesEntity,
      CategoryEntity,
    ]),
  ],
  controllers: [PostsController],
  providers: [
    PostsUseCases,
    { provide: IUsersRepository, useClass: UsersRepository },
    { provide: IPostsRepository, useClass: PostsRepository },
  ],
})
export class PostsModule {}
