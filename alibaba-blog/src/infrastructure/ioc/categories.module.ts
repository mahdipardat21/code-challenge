import { CategoryEntity } from "./../database/mapper/CategoryEntity";
import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { CategoriesUseCase } from "src/application/use-cases/CategoriesUseCase";
import { ICategoriesRepository } from "src/application/ports/ICategoriesRepository";
import { CategoriesRepository } from "../database/repositories/CategoriesRepository";
import { CategoriesController } from "src/presentation/controllers/CategoriesController";
import { PostCategoriesEntity } from "../database/mapper/PostCategoriesEntity";

@Module({
  imports: [SequelizeModule.forFeature([CategoryEntity, PostCategoriesEntity])],
  controllers: [CategoriesController],
  providers: [
    CategoriesUseCase,
    {
      provide: ICategoriesRepository,
      useClass: CategoriesRepository,
    },
  ],
})
export class CategoriesModule {}
