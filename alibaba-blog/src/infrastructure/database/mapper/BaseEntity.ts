import { Model, Column, DataType } from "sequelize-typescript";

export class BaseEntity extends Model {
    @Column({ type: DataType.BIGINT, autoIncrement: true, primaryKey: true})
    id: number;

    @Column({type: DataType.DATE, field: 'created_at'})
    createdAt: Date;

    @Column({type: DataType.DATE, field: 'updated_at'})
    updatedAt: Date;
}
