import { BaseEntity } from "./BaseEntity";
import {
  Table,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  BelongsToMany,
} from "sequelize-typescript";
import { UserEntity } from "./UserEntity";
import { CategoryEntity } from "./CategoryEntity";
import { PostCategoriesEntity } from "./PostCategoriesEntity";

@Table({ tableName: "posts" })
export class PostEntity extends BaseEntity {
  @Column({ type: DataType.STRING(255) })
  title: string;

  @Column({ type: DataType.TEXT })
  text: string;

  @Column({ type: DataType.BIGINT, field: "user_id" })
  @ForeignKey(() => UserEntity)
  userId: number;

  @BelongsTo(() => UserEntity, { onDelete: "CASCADE", onUpdate: "CASCADE" })
  user: UserEntity;

  @BelongsToMany(() => CategoryEntity, () => PostCategoriesEntity)
  categories: CategoryEntity[];
}
