import { BelongsToMany, Column, DataType, Table } from "sequelize-typescript";
import { BaseEntity } from "./BaseEntity";
import { PostCategoriesEntity } from "./PostCategoriesEntity";
import { PostEntity } from "./PostEntity";



@Table({ tableName: 'categories'})
export class CategoryEntity extends BaseEntity {
    @Column({ type: DataType.STRING(255), unique: true})
    title: string;

    @BelongsToMany(() => PostEntity, () => PostCategoriesEntity)
    posts: PostEntity[];
}