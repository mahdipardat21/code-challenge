import { Column, DataType, ForeignKey, Table } from "sequelize-typescript";
import { BaseEntity } from "./BaseEntity";
import { CategoryEntity } from "./CategoryEntity";
import { PostEntity } from "./PostEntity";


@Table({tableName: 'posts_categories'})
export class PostCategoriesEntity extends BaseEntity {
    @ForeignKey(() => PostEntity)
    @Column({ type: DataType.BIGINT, field: 'post_id'})
    postId: number;


    @ForeignKey(() => CategoryEntity)
    @Column({ type: DataType.BIGINT, field: 'category_id'})
    categoryId: number;
}