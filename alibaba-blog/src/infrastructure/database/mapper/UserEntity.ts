import { BaseEntity } from "./BaseEntity";
import { Table, Column, DataType, HasMany } from 'sequelize-typescript'
import { PostEntity } from "./PostEntity";

@Table({ tableName: 'users'})
export class UserEntity extends BaseEntity {
    @Column({ type: DataType.STRING(255)})
    name: string;

    @Column({ type: DataType.STRING(255)})
    email: string;

    @HasMany(() => PostEntity, { onDelete: 'CASCADE', onUpdate: 'CASCADE'})
    posts: PostEntity[];
}