import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { FindOptions, NonNullFindOptions } from "sequelize/types";
import { IUsersRepository } from "src/application/ports/IUsersRepository";
import { Post } from "src/domain/models/Post";
import { User } from "src/domain/models/User";
import { UserEntity } from "../mapper/UserEntity";

@Injectable()
export class UsersRepository implements IUsersRepository {
  constructor(@InjectModel(UserEntity) private userEntity: typeof UserEntity) {}

  async find(options?: FindOptions): Promise<User[]> {
    const users = await this.userEntity.findAll(options);
    return users.map((user) => this.mapEntityToUser(user));
  }
  async findOne(id: number, options?: NonNullFindOptions): Promise<User> {
    const user = await this.userEntity.findByPk(id, options);
    return this.mapEntityToUser(user);
  }
  async save(entity: User, options?: any): Promise<User> {
    const savedUser = await this.userEntity.create({
      name: entity.name,
      email: entity.email,
    });
    return this.mapEntityToUser(savedUser);
  }
  async update(
    criteria: any,
    partialEntity: Partial<Omit<User, "id">>
  ): Promise<any> {
    const [affected] = await this.userEntity.update(partialEntity, {
      where: { ...criteria },
    });
    return affected;
  }
  async delete(criteria: any): Promise<number> {
    return await this.userEntity.destroy({ where: { ...criteria } });
  }

  async findUser(user: User): Promise<User> {
    const findUser = await this.userEntity.findOne({
      where: { email: user.email },
    });
    return this.mapEntityToUser(findUser);
  }

  private mapEntityToUser(userEntity: UserEntity): User {
    if (userEntity) {
      const posts =
        userEntity.posts?.map(
          (p) => new Post(p.title, p.text, undefined, [], p.id)
        ) ?? [];
      return new User(userEntity.name, userEntity.email, posts, userEntity.id);
    }
  }
}
