import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { ICategoriesRepository } from "src/application/ports/ICategoriesRepository";
import { Category } from "src/domain/models/Category";
import { CategoryEntity } from "../mapper/CategoryEntity";

@Injectable()
export class CategoriesRepository implements ICategoriesRepository {
  constructor(
    @InjectModel(CategoryEntity) private categoryEntity: typeof CategoryEntity
  ) {}

  async save(category: Category): Promise<Category> {
    const savedCategory = await this.categoryEntity.create({
      title: category.title,
    });
    return this.mapEntityToCategory(savedCategory);
  }

  async find(): Promise<Category[]> {
    const categories = await this.categoryEntity.findAll();
    return categories.map((cat) => this.mapEntityToCategory(cat));
  }

  async findOne(id: number): Promise<Category> {
    const category = await this.categoryEntity.findByPk(id);
    return this.mapEntityToCategory(category);
  }

  async update(id: number, partial: Partial<Category>): Promise<number> {
    const [affected] = await this.categoryEntity.update(partial, {
      where: { id },
    });
    return affected;
  }

  async delete(id: number): Promise<number> {
    return await this.categoryEntity.destroy({ where: { id } });
  }

  private mapEntityToCategory(categoryEntity: CategoryEntity): Category {
    if (categoryEntity) {
      return new Category(categoryEntity.title, categoryEntity.id);
    }
  }
}
