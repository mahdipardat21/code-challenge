import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Op, Sequelize } from "sequelize";
import { IPostsRepository } from "src/application/ports/IPostsRepository";
import { Category } from "src/domain/models/Category";
import { Post } from "src/domain/models/Post";
import { User } from "src/domain/models/User";
import { SortQuery } from "src/infrastructure/utils/query/BaseSearchQuery";
import { CategoryEntity } from "../mapper/CategoryEntity";
import { PostCategoriesEntity } from "../mapper/PostCategoriesEntity";
import { PostEntity } from "../mapper/PostEntity";
import { UserEntity } from "../mapper/UserEntity";

@Injectable()
export class PostsRepository implements IPostsRepository {
  constructor(
    @InjectModel(PostEntity) private readonly postEntity: typeof PostEntity,
    @InjectModel(PostCategoriesEntity)
    private readonly postCategoryEntity: typeof PostCategoriesEntity // private readonly sequelize: Sequelize
  ) {}

  async count(term: string): Promise<number> {
    const where = this.termQuery(term);
    return await this.postEntity.count({ where });
  }

  async find(
    term: string,
    perPage: number,
    offset: number,
    sort: SortQuery
  ): Promise<Post[]> {
    const where = this.termQuery(term);
    const condition = {
      where,
      include: [{ model: UserEntity }, { model: CategoryEntity }],
    };

    if (perPage) {
      Object.assign(condition, { limit: perPage });
    }

    if (offset) {
      Object.assign(condition, { offset });
    }

    if (sort) {
      Object.assign(condition, { order: [["updatedAt", sort]] });
    }
    const posts = await this.postEntity.findAll(condition);
    return posts.map((p) => this.mapEntityToPost(p));
  }
  async findOne(id: number): Promise<Post> {
    const post = await this.postEntity.findByPk(id, {
      include: [{ model: UserEntity }, { model: CategoryEntity }],
    });
    console.log(post.categories);
    return this.mapEntityToPost(post);
  }
  async save(post: Post): Promise<Post> {
    const createdPost = await this.postEntity.create({
      title: post.title,
      text: post.text,
      userId: post.user?.id,
    });

    if (createdPost) {
      const categories = post.categories?.map((cat) => ({
        categoryId: cat.id,
        postId: createdPost.id,
      }));
      await this.postCategoryEntity.bulkCreate(categories);
    }

    return this.findOne(createdPost.id);
  }
  async update(post: Post): Promise<number> {
    const findPost = await this.postEntity.findOne({
      where: { id: post.id, userId: post.user.id },
      include: [{ model: CategoryEntity }],
    });

    if (!findPost || findPost.categories?.length > 6) {
      return 0;
    }

    const updated = await findPost.update({
      title: post.title ?? undefined,
      text: post.text ?? undefined,
    });

    if (updated && post.categories?.length > 0) {
      await this.postCategoryEntity.destroy({
        where: {
          postId: findPost.id,
          categoryId: { [Op.in]: findPost.categories.map((cat) => cat.id) },
        },
      });

      const createdCategory = post.categories.map((cat) => ({
        postId: findPost.id,
        categoryId: cat.id,
      }));

      await this.postCategoryEntity.bulkCreate(createdCategory);
    }

    return 1;
  }
  delete(post: Post): Promise<number> {
    return this.postEntity.destroy({
      where: { id: post.id, userId: post.user.id },
    });
  }

  private termQuery(term: string): Record<string, string> {
    return term
      ? {
          [Op.or]: {
            title: {
              [Op.like]: `%${term}%`,
            },
            text: {
              [Op.like]: `%${term}%`,
            },
          },
        }
      : {};
  }

  private mapEntityToPost(postEntity: PostEntity): Post {
    if (postEntity) {
      const user = postEntity.user
        ? new User(
            postEntity.user.name,
            postEntity.user.email,
            [],
            postEntity.user.id
          )
        : null;
      const categories =
        postEntity.categories?.map((cat) => new Category(cat.title, cat.id)) ??
        [];
      return new Post(
        postEntity.title,
        postEntity.text,
        user,
        categories,
        postEntity.id
      );
    }
  }
}
