import { ApiProperty } from "@nestjs/swagger";

export class GetPaginationData<T> {
  @ApiProperty()
  totalItems: number;

  @ApiProperty()
  items: T[];
}
