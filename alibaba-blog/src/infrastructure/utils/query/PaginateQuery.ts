import { ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNumber, IsOptional, Min } from "class-validator";


export class PaginateQuery {
    @ApiPropertyOptional()
    @Transform((num) => +num.value || +num)
    @IsOptional()
    @IsNumber()
    perPage?: number;

    @ApiPropertyOptional()
    @Transform((num) => +num.value || +num)
    @IsOptional()
    @IsNumber()
    @Min(0)
    page?: number = 1;
}