import { IsEnum, IsOptional, IsString } from "class-validator";
import { ApiPropertyOptional } from "@nestjs/swagger";
import { PaginateQuery } from "./PaginateQuery";

export enum SortQuery {
  asc = "asc",
  desc = "desc",
}

export class BaseSearchQuery extends PaginateQuery {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  term?: string;

  @ApiPropertyOptional({ enum: SortQuery })
  @IsOptional()
  @IsEnum(SortQuery)
  sort: SortQuery = SortQuery.desc;
}
