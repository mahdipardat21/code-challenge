import { HttpException } from '@nestjs/common';
import { Sequelize } from 'sequelize/types';

export function Transactional() {
  return function (
    target,
    propertyKey: string,
    descriptor: PropertyDescriptor,
  ) {
    const original = descriptor.value;
    descriptor.value = async function (...args: any) {
      const sequelize: Sequelize = this.sequelize;
      const trans = await sequelize.transaction();
      try {
        const result = await original.apply(this, args);
        await trans.commit();
        return result;
      } catch (error) {
        await trans.rollback();
        if (error instanceof HttpException) {
          throw new HttpException(error.getResponse(), error.getStatus());
        }
        return error;
      }
    };
  };
}
