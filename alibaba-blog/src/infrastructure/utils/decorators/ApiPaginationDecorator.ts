import { applyDecorators, Type } from "@nestjs/common";
import { ApiOkResponse, getSchemaPath } from "@nestjs/swagger";
import { GetPaginationData } from "../dto/GetPaginationData";


export const ApiPaginatedResponse = <TModel extends Type<any>>(
	model: TModel,
) => {
	return applyDecorators(
		ApiOkResponse({
			schema: {
				allOf: [
					{ $ref: getSchemaPath(GetPaginationData) },
					{
						properties: {
							totalItem: {
								type: 'number',
								example: 20
							},
							items   : {
								type: 'array',
								items: { $ref: getSchemaPath(model) },
							}
						},
					},
				],
			},
		}),
	);
};


