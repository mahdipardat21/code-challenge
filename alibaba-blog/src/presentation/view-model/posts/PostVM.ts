import { CategoryVM } from "./../categories/CategoryVM";
import { ApiProperty } from "@nestjs/swagger";
import { Post } from "src/domain/models/Post";
import { User } from "src/domain/models/User";

export class UserVM {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  email: string;

  constructor(name: string, email: string, id: number) {
    this.name = name;
    this.email = email;
    this.id = id;
  }

  static fromUser(user: User): UserVM {
    if (user) {
      return new UserVM(user.name, user.email, +user.id);
    }
  }
}

export class PostVM {
  @ApiProperty()
  id: number;

  @ApiProperty()
  title: string;

  @ApiProperty()
  text: string;

  @ApiProperty({ type: UserVM })
  user: UserVM;

  @ApiProperty({ type: [CategoryVM] })
  categories: CategoryVM[];

  constructor(
    title: string,
    text: string,
    user: UserVM,
    categories: CategoryVM[],
    id: number
  ) {
    this.title = title;
    this.text = text;
    this.id = id;
    this.user = user;
    this.categories = categories;
  }

  static fromPost(post: Post): PostVM {
    return new PostVM(
      post.title,
      post.text,
      UserVM.fromUser(post.user) ?? null,
      post.categories.map((cat) => CategoryVM.fromCategory(cat)) ?? [],
      +post.id
    );
  }
}
