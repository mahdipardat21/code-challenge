import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsOptional, IsString } from "class-validator";



export class UpdatePostVM {
    @ApiProperty()
    @IsOptional()
    @IsString()
    title: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    text: string;

    @ApiProperty()
    @IsOptional()
    @IsArray()
    categories: number[];
}