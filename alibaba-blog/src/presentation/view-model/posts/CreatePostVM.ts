import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsString } from "class-validator";


export class CreatePostVM {
    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsString()
    text: string;

    @ApiProperty()
    @IsArray()
    categories: number[];
}