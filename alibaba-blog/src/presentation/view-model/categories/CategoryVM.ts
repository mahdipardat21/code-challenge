import { ApiProperty } from "@nestjs/swagger";
import { Category } from "src/domain/models/Category";

export class CategoryVM {
  @ApiProperty()
  id: number;

  @ApiProperty()
  title: string;

  constructor(id: number, title: string) {
    this.id = +id;
    this.title = title;
  }

  static fromCategory(category: Category): CategoryVM {
    return new CategoryVM(category.id, category.title);
  }
}
