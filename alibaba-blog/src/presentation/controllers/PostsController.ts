import { PostsUseCases } from "src/application/use-cases/PostsUseCases";
import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Query,
  Post,
  UseGuards,
  Delete,
  Put,
} from "@nestjs/common";
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { BaseSearchQuery } from "src/infrastructure/utils/query/BaseSearchQuery";
import { GetPaginationData } from "src/infrastructure/utils/dto/GetPaginationData";
import { ApiPaginatedResponse } from "src/infrastructure/utils/decorators/ApiPaginationDecorator";
import { CurrentUser } from "src/infrastructure/utils/decorators/CurrentUser";
import { User } from "src/domain/models/User";
import { Post as PostModel } from "src/domain/models/Post";
import { PostVM } from "../view-model/posts/PostVM";
import { CreatePostVM } from "../view-model/posts/CreatePostVM";
import { Category } from "src/domain/models/Category";
import { AuthGuard } from "src/infrastructure/utils/guards/AuthGuard";
import { UpdatePostVM } from "../view-model/posts/UpdatePostVM";

@Controller("posts")
@ApiTags("Post")
export class PostsController {
  constructor(private readonly postsUseCase: PostsUseCases) {}

  @Get("")
  @ApiOperation({ description: "fetch all posts" })
  @ApiPaginatedResponse(PostVM)
  async getMany(
    @Query() query: BaseSearchQuery
  ): Promise<GetPaginationData<PostVM>> {
    const posts = await this.postsUseCase.findPosts(query);
    const totalItems = await this.postsUseCase.countPosts(query.term);
    const items = posts.map((post) => PostVM.fromPost(post));
    return {
      totalItems,
      items,
    };
  }

  @Get(":id")
  @ApiOperation({ description: "get on posts" })
  @ApiOkResponse({ type: PostVM })
  async getOne(@Param("id", ParseIntPipe) id: number): Promise<PostVM> {
    const post = await this.postsUseCase.findPost(id);
    return PostVM.fromPost(post);
  }

  @Post("")
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "create a post for a user logged in fake user" })
  @ApiOkResponse({ type: PostVM })
  async create(
    @Body() createPostVM: CreatePostVM,
    @CurrentUser() user: User
  ): Promise<PostVM> {
    const post = await this.postsUseCase.createPost(
      user.id,
      new PostModel(
        createPostVM.title,
        createPostVM.text,
        null,
        createPostVM.categories.map((id) => new Category(null, id))
      )
    );
    return PostVM.fromPost(post);
  }

  @Put(":id")
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "update a post for logged user in fake token" })
  @ApiOkResponse({ type: Boolean })
  async update(
    @Param("id", ParseIntPipe) id: number,
    @Body() updatePostVM: UpdatePostVM,
    @CurrentUser() user: User
  ): Promise<boolean> {
    return this.postsUseCase.updatePost(
      user.id,
      new PostModel(
        updatePostVM.title,
        updatePostVM.text,
        user,
        updatePostVM.categories?.map((id) => new Category(null, id)),
        id
      )
    );
  }

  @Delete(":id")
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "delete a post for logged user in fake token" })
  @ApiOkResponse({ type: Boolean })
  async delete(
    @Param("id", ParseIntPipe) id: number,
    @CurrentUser() user: User
  ): Promise<boolean> {
    return this.postsUseCase.deletePost(
      new PostModel(null, null, user, [], id)
    );
  }
}
