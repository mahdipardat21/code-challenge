import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from "@nestjs/common";
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { CategoriesUseCase } from "src/application/use-cases/CategoriesUseCase";
import { Category } from "src/domain/models/Category";
import { AuthGuard } from "src/infrastructure/utils/guards/AuthGuard";
import { CategoryVM } from "../view-model/categories/CategoryVM";
import { CreateCategoryVM } from "../view-model/categories/CreateCategoryVM";

@Controller("categories")
@ApiTags("Categories")
export class CategoriesController {
  constructor(private readonly categoriesUseCase: CategoriesUseCase) {}

  @Get("")
  @ApiOperation({ description: "fetch all categories" })
  @ApiOkResponse({ type: [CategoryVM] })
  async getAll(): Promise<CategoryVM[]> {
    const categories = await this.categoriesUseCase.findCategories();
    return categories.map((cat) => CategoryVM.fromCategory(cat));
  }

  @Get(":id")
  @ApiOperation({ description: "get one category" })
  @ApiOkResponse({ type: CategoryVM })
  async getOne(@Param("id", ParseIntPipe) id: number): Promise<CategoryVM> {
    const category = await this.categoriesUseCase.findCategory(id);
    return CategoryVM.fromCategory(category);
  }

  @Post()
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "create a category" })
  @ApiOkResponse({ type: CategoryVM })
  async create(@Body() categoryVM: CreateCategoryVM): Promise<CategoryVM> {
    const category = new Category(categoryVM.title);
    const saved = await this.categoriesUseCase.createCategory(category);
    return CategoryVM.fromCategory(saved);
  }

  @Put(":id")
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "update a category" })
  @ApiOkResponse({ type: Boolean })
  async update(
    @Param("id", ParseIntPipe) id: number,
    @Body() categoryVM: CreateCategoryVM
  ): Promise<boolean> {
    return await this.categoriesUseCase.update(
      new Category(categoryVM.title, id)
    );
  }

  @Delete(":id")
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ description: "delete a category" })
  @ApiOkResponse({ type: Boolean })
  async delete(@Param("id", ParseIntPipe) id: number): Promise<boolean> {
    return await this.categoriesUseCase.delete(id);
  }
}
