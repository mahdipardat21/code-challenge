import { HttpExceptionFilter } from "./infrastructure/rest/http.exception.filter";
import { LogginInterceptor } from "./infrastructure/rest/logging.interceptor";
import { INestApplication, Logger } from "@nestjs/common";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ConfigService } from "@nestjs/config";

import * as bodyParser from "body-parser";
import * as chalk from "chalk";
import * as compression from "compression";
import * as rateLimit from "express-rate-limit";
import * as helmet from "helmet";
import { ValidationPipe } from "./infrastructure/rest/validation.pipe";

declare const module: any;

const bootstrap = async () => {
  try {
    const app = await NestFactory.create<INestApplication>(AppModule, {
      cors: true,
    });

    const configService = app.get(ConfigService);
    Logger.log(
      `Environment: ${chalk
        .hex("#1B4353")
        .bold(`${process.env.NODE_ENV?.toUpperCase()}`)}`,
      "Bootstrap"
    );

    app.use(helmet());
    app.use(compression());
    app.use(
      bodyParser.urlencoded({
        limit: "50mb",
        extended: true,
        parameterLimit: 50000,
      })
    );
    app.use(
      rateLimit({
        windowMs: 1000 * 60 * 60,
        max: 1000,
        message: "Too many request created from this IP",
      })
    );

    

    app.useGlobalInterceptors(new LogginInterceptor());
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalPipes(new ValidationPipe());

    const APP_NAME = configService.get("APP_NAME");
    const APP_DESCRIPTION = configService.get("APP_DESCRIPTION");
    const API_VERSION = configService.get("API_VERSION");

    const options = new DocumentBuilder()
      .setTitle(APP_NAME)
      .setDescription(APP_DESCRIPTION)
      .setVersion(API_VERSION)
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup("api", app, document);
    SwaggerModule.setup("/", app, document);

    Logger.log("Mapped {/, GET} Swagger api route", "RouterExplorer");
    Logger.log("Mapped {/api, GET} Swagger api route", "RouterExplorer");

    const HOST = configService.get("HOST");
    const PORT = configService.get("PORT");

    await app.listen(PORT);
    process.env.NODE_ENV !== "production"
      ? Logger.log(
          `🚀  Server ready at http://${HOST}:${chalk
            .hex("#87e8de")
            .bold(`${PORT}`)}`,
          "Bootstrap",
          false
        )
      : Logger.log(
          `🚀  Server is listening on port ${chalk
            .hex("#87e8de")
            .bold(`${PORT}`)}`,
          "Bootstrap",
          false
        );

    if (module.hot) {
      module.hot.accept();
      module.hot.dispose(() => app.close());
    }
  } catch (error) {
    Logger.error(`❌  Error starting server, ${error}`, "", "Bootstrap", false);
    process.exit();
  }
};

bootstrap().catch((e) => {
  Logger.error(`❌  Error starting server, ${e}`, "", "Bootstrap", false);
  throw e;
});
