import { PostsModule } from "./infrastructure/ioc/posts.module";
import { UsersModule } from "./infrastructure/ioc/users.module";
import {
  CacheInterceptor,
  CacheModule,
  MiddlewareConsumer,
  Module,
  NestModule,
  OnModuleInit,
} from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { setEnvironment } from "./infrastructure/environments";
import { CacheService } from "./infrastructure/cache";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { SequelizeModule } from "@nestjs/sequelize";

import { CategoriesModule } from "./infrastructure/ioc/categories.module";
import { UsersUseCases } from "./application/use-cases/UsersUseCases";
import { User } from "./domain/models/User";
import { request, Request, Response } from "express";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
      envFilePath: setEnvironment(),
    }),
    CacheModule.registerAsync({
      useClass: CacheService,
    }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        dialect: configService.get("DB_TYPE"),
        port: configService.get("DB_PORT"),
        host: configService.get("DB_HOST"),
        username: configService.get("DB_USER"),
        password: configService.get("DB_PASS"),
        database: configService.get("DB_DATABASE"),
        autoLoadModels: true,
        sync: { force: true },
        logging: true,
      }),
    }),
    UsersModule,
    PostsModule,
    CategoriesModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  constructor(private readonly usersUseCase: UsersUseCases) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(async (req: Request, res: Response, next: Function) => {
        const user = new User("Mahdi", "pardatdev@gmail.com");

        let authUser = await this.usersUseCase.findOneUser(user);

        if (!authUser) {
          authUser = await this.usersUseCase.createUser(user);
        }

        if (req.get('Authorization')) {
          req["user"] = authUser;
        }

        next();
      })
      .forRoutes("*");
  }
}
