import { Injectable, NotFoundException } from "@nestjs/common";
import { Category } from "src/domain/models/Category";
import { ICategoriesRepository } from "../ports/ICategoriesRepository";

@Injectable()
export class CategoriesUseCase {
  constructor(private readonly categoriesRepository: ICategoriesRepository) {}

  findCategories(): Promise<Category[]> {
    return this.categoriesRepository.find();
  }

  async findCategory(categoryId: number): Promise<Category> {
    const category = await this.categoriesRepository.findOne(categoryId);
    if (!category)
      throw new NotFoundException(`category with id ${categoryId} not found.`);
    return category;
  }

  createCategory(category: Category): Promise<Category> {
    return this.categoriesRepository.save(category);
  }

  async update(category: Category): Promise<boolean> {
    const affected = await this.categoriesRepository.update(
      category.id,
      category
    );
    return affected > 0;
  }

  async delete(categoryId: number): Promise<boolean> {
    const affected = await this.categoriesRepository.delete(categoryId);
    return affected > 0;
  }
}
