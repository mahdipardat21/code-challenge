import { IUsersRepository } from "./../ports/IUsersRepository";
import { Injectable, Logger, NotFoundException } from "@nestjs/common";
import { User } from "src/domain/models/User";

@Injectable()
export class UsersUseCases {
  private readonly logger = new Logger(UsersUseCases.name);

  constructor(private readonly usersRepostory: IUsersRepository) {}

  async getUsers(): Promise<User[]> {
    this.logger.log("Find all users");

    return await this.usersRepostory.find({});
  }

  async getUserById(id: number): Promise<User> {
    this.logger.log(`Find the user ${id}`);

    const user = await this.usersRepostory.findOne(id);
    if (!user) throw new NotFoundException(`The user with id ${id} not found.`);

    return user;
  }

  async createUser(user: User): Promise<User> {
    this.logger.log("create a user");
    return await this.usersRepostory.save(user);
  }

  async updateUser(user: User): Promise<boolean> {
    this.logger.log(`Updating user with id ${user.id}`);
    const result = await this.usersRepostory.update({ id: user.id }, user);
    return result > 0;
  }

  async deleteUser(user: User): Promise<boolean> {
    this.logger.log(`Deleting user with id ${user.id}`);
    const result = await this.usersRepostory.delete({ id: user.id });
    return result > 0;
  }

  findOneUser(user: User): Promise<User> {
    return this.usersRepostory.findUser(user);
  }
}
