import { BadRequestException, Injectable, Logger, NotFoundException } from "@nestjs/common";
import { Post } from "src/domain/models/Post";
import { Transactional } from "src/infrastructure/utils/decorators/Transactional";
import { BaseSearchQuery } from "src/infrastructure/utils/query/BaseSearchQuery";
import { IPostsRepository } from "../ports/IPostsRepository";
import { IUsersRepository } from "../ports/IUsersRepository";

@Injectable()
export class PostsUseCases {
  private readonly logger = new Logger(PostsUseCases.name);

  constructor(
    private readonly usersRepository: IUsersRepository,
    private readonly postsRepository: IPostsRepository
  ) {}

  async findPosts(query: BaseSearchQuery): Promise<Post[]> {
    const { perPage, page, term, sort } = query;
    const offset = (page - 1) * perPage;
    return await this.postsRepository.find(term, perPage, offset, sort);
  }

  async countPosts(term: string): Promise<number> {
    return await this.postsRepository.count(term);
  }

  async findPost(postId: number): Promise<Post> {
    const post = await this.postsRepository.findOne(postId);

    if (!post) throw new NotFoundException(`Post with id ${postId} not found`);

    return post;
  }

  // @Transactional()
  async createPost(userId: number, post: Post): Promise<Post> {
    this.logger.log(`Create post for user ${userId}`);

    const user = await this.usersRepository.findOne(userId);

    if (!user)
      throw new NotFoundException(`The user {${userId}} wasn't found.`);

    if (post.categories?.length > 6) {
      throw new BadRequestException(`Maximum count of categories must be 6`)
    }

    post.user = user;

    const savedPost = await this.postsRepository.save(post);

    return savedPost;
  }

  async updatePost(userId: number, post: Post): Promise<boolean> {
    this.logger.log(`Update post for user ${userId}`);

    const user = await this.usersRepository.findOne(userId);

    if (!user)
      throw new NotFoundException(`The user {${userId}} wasn't found.`);

    const findPost = await this.findPost(post.id);

    if (!findPost || post.user.id != userId) {
      throw new NotFoundException(`You can't update this post`);
    }

    post.user = user;

    const affected = await this.postsRepository.update(post)

    return affected > 0;

  }

  async deletePost(post: Post): Promise<boolean> {
    const affected = await this.postsRepository.delete(post);
    return affected > 0;
  }
}
