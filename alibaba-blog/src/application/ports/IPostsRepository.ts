import { FindOptions } from "sequelize";
import { Post } from "src/domain/models/Post";
import { SortQuery } from "src/infrastructure/utils/query/BaseSearchQuery";

export abstract class IPostsRepository {
  abstract find(
    term: string,
    perPage: number,
    offset: number,
    sort: SortQuery
  ): Promise<Post[]>;
  abstract findOne(id: number, options?: FindOptions): Promise<Post>;
  abstract count(term: string): Promise<number>;
  abstract save(post: Post): Promise<Post>;
  abstract update(post: Post): Promise<number>;
  abstract delete(post: Post): Promise<number>;
}
