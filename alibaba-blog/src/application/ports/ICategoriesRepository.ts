import { Category } from "src/domain/models/Category";


export abstract class ICategoriesRepository {
    abstract save(category: Category): Promise<Category>;
    abstract find(): Promise<Category[]>;
    abstract findOne(id: number): Promise<Category>;
    abstract update(id: number, partial: Partial<Category>): Promise<number>;
    abstract delete(id: number): Promise<number>;
}