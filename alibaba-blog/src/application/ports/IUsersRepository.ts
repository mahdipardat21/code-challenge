import { FindOptions, NonNullFindOptions } from "sequelize/types";
import { User } from "src/domain/models/User";


export abstract class IUsersRepository {
    abstract find(options?: FindOptions): Promise<User[]>;
    abstract findOne(id: number, options?: NonNullFindOptions): Promise<User>;
    abstract save(entity: User, options?: any): Promise<User>;
    abstract update(criteria: any, partialEntity: Partial<Omit<User, 'id'>>): Promise<number>;
    abstract delete(criteria: any): Promise<number>;
    abstract findUser(user: User): Promise<User>;
}